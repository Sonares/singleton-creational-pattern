#Singleton
This is a simple example of Singleton, creational design pattern.  

**Description:**  
The singleton pattern ensures that only one object of a particular class is ever created. All further references to objects of the singleton class refer to the same underlying instance.  

![PlantUML model] (http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLWZEp4lFIIt9prEevjAj1agMP2RNPkO16Nd9gLO8QIvTEDDAmJaVgA2eDBaaluXB6wOfFRKa6CXArT14rbRBvP2QbmAq3m00)  
