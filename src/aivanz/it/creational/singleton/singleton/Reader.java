package aivanz.it.creational.singleton.singleton;

public class Reader {
	private static volatile Reader instance = null;

	private Reader() {
	}

	// Double control to avoid synchronized overhead
	public static synchronized Reader getState() {
		if (instance == null)
			synchronized (Reader.class) {
				if (instance == null)
					instance = new Reader();
			}

		return instance;
	}

	// State Information
	private String loginId;
	public int maxSize;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

}
