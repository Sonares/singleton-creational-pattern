package aivanz.it.creational.singleton;

import aivanz.it.creational.singleton.singleton.Reader;

/* Creational pattern, it controls class instantiation. 
 * The pattern ensures that only one object of a particular class is ever created. 
 * All further references to objects of the singleton class refer to the same underlying instance. */

/* Example:
 * Reader. 
 * A reader class is defined to hold the application's state. Two properties are required, one for the 
 * user's login details and one for the maximum size of some element that they can manipulate. To ensure 
 * that the state is only held in a single instance, the class uses the singleton pattern approach. */

/* Singleton:
 * -instance: static Singleton
 * -Singleton(): Constructor
 * +getState(): Singleton
 * In this case: Reader.
 * This class lets you instantiate only 1 object, further instantiations reference all to the first one. 
 * It's a "global variable", so must be thread safe (thanks to volatile declaration of "instance" and 
 * syncronized getState() method). */

public class MainClass {
	public static void main(String args[]) {

		Reader state = Reader.getState();
		state.setLoginId("admin");
		state.setMaxSize(1024);

		Reader state2 = Reader.getState();
		System.out.println(state2.getLoginId());
		System.out.println(state2.getMaxSize());
		System.out.println(state == state2);

	}
}
